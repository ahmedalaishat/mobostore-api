const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const passport = require('passport');
const init = require('./api/init');
const usersRouter = require('./api/routes/users');
const brandRouter = require('./api/routes/brands');
const phoneRouter = require('./api/routes/mobiles');
const GSMRouter = require('./api/routes/gsm');

init.database();
init.auth();

const server = restify.createServer();
const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['*'],
});

server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser({ mapParams: true }));
server.use(restify.plugins.requestLogger());
server.use(passport.initialize());

usersRouter.applyRoutes(server, '/users');
brandRouter.applyRoutes(server, '/brands');
phoneRouter.applyRoutes(server, '/mobiles');
GSMRouter.applyRoutes(server, '/gsmarena');

server.on('restifyError', function (req, res, err, cb) {
    err.toJSON = function toJSON() {
        return {
            ...err.body,
            statusCode:err.statusCode
        }
    };
    return cb();
});

server.listen(process.env.PORT || 8888, function () {
    console.log('%s listening at %s', server.name, server.url);
});