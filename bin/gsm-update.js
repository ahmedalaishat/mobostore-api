/**
 * Created by Ahmed Al-Aishat on Aug/19/2022.
 * mobostore-api Project.
 * Copyright (c) 2022 Cloud Systems. All rights reserved.
 */
const init = require('../api/init');
init.database();
const {updateMobs, updateDetails} = require('../api/services/gsm');
const History = require('../api/models/History');


const update = async () => {
    const updateMobsResult = await updateMobs()
    updateMobsResult.brands = updateMobsResult.brands.map(brandResult => {
        const {mobiles, ...result} = brandResult.result
        brandResult.result = result
        return brandResult
    })

    const updateDetailsResult = await updateDetails()
    return {
        updateMobsResult,
        updateDetailsResult,
    }
}

update().then((result) => History.create({result}))