/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/8/2020
 * Time: 6:28 PM
 */
const brandController = require('../controllers/brands');
const auth = require('../midlewares/auth');
const Router = require('restify-router').Router;
const router = new Router();

router.get('/',auth.verifyUser, brandController.index);
router.get('/:id',auth.verifyUser, brandController.show);
router.post('/',auth.verifyAdmin, brandController.store);
router.put('/:id',auth.verifyAdmin, brandController.update);
router.del('/:id',auth.verifyAdmin, brandController.destroy);

module.exports = router;