/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/8/2020
 * Time: 6:30 PM
 */
const mobilesController = require('../controllers/mobiles');
const auth = require('../midlewares/auth');
const Router = require('restify-router').Router;
const router = new Router();

router.get('/', auth.verifyUser, mobilesController.index);
router.get('/:id',auth.verifyUser, mobilesController.show);
router.post('/',auth.verifyAdmin, mobilesController.store);
router.put('/:id',auth.verifyAdmin, mobilesController.update);
router.del('/:id',auth.verifyAdmin, mobilesController.destroy);
// router.get('/search/:mobile', mobilesController.search);
// router.get('/reviews/', mobilesController.indexReviews);
// router.get('/reviews/:url', mobilesController.showReview);

module.exports = router;