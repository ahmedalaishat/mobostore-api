/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/9/2020
 * Time: 6:40 PM
 */
const GSMController = require('../controllers/gsm');
const auth = require('../midlewares/auth');
const Router = require('restify-router').Router;
const router = new Router();

// TODO optimize prints and returns
router.post('/brands',auth.verifyAdmin, GSMController.updateAllBrands);
router.post('/mobs',auth.verifyAdmin, GSMController.updateBrandsMobs);
router.post('/mobs-details',auth.verifyAdmin, GSMController.updateMobsDetails);

module.exports = router;