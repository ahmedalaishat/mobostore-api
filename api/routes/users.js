/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 6/9/2020
 * Time: 6:15 PM
 */
const usersController = require('../controllers/users');
const Router = require('restify-router').Router;
const auth = require('../midlewares/auth');
const router = new Router();

router.post('/sign-up', usersController.signUp);
router.post('/sign-in',auth.authenticate, usersController.signIn);

module.exports = router;