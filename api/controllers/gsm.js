/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/9/2020
 * Time: 6:41 PM
 */
const {updateBrands, updateMobs, updateDetails} = require('../services/gsm');

/**
 * get all brands from gsmarena and store them in the database
 */
const updateAllBrands = async (req, res, next) => {
    try {
        const creationResult = await updateBrands(req?.body?.acceptable)
        res.send(200, {
            status: 'success',
            message: "Brands updated successfully.",
            creationResult,
        });
    } catch (e) {
        console.log(JSON.stringify(e))
        return next(e);
    }
};
/**
 * get all mobiles from a specific brand and add not existing mobile to the database
 */
const updateBrandsMobs = async (req, res, next) => {
    try {
        const result = await updateMobs(req?.body?.filter)
        res.send(200, {
            status: 'success',
            message: "Mobiles updated successfully.",
            result,
        });
    } catch (e) {
        next(e)
    }
};
/**
 * get details for all non-completed mobiles and update them
 */
const updateMobsDetails = async (req, res, next) => {
    try {
        const result = await updateDetails(req?.body?.filter)
        res.send(200, {
            status: 'success',
            message: "Mobiles details updated successfully.",
            result,
        });
    } catch (e) {
        next(e)
    }
};

module.exports = {
    updateAllBrands,
    updateBrandsMobs,
    updateMobsDetails,
};