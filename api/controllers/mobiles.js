/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/9/2020
 * Time: 2:51 PM
 */
const {Mobile} = require('../models/Mobile');
// TODO add search
module.exports = {
    index(req, res, next) {
        const page = Number(req.query.page) || 1;
        const myCustomLabels = {
            totalDocs: 'itemCount',
            docs: 'mobiles',
            limit: 'perPage',
            page: 'currentPage',
            nextPage: 'next',
            prevPage: 'prev',
            totalPages: 'pageCount',
            pagingCounter: 'slNo',
            meta: 'paginator'
        };
        const options = {
            select: '_id name title url img img_url description price brand',
            page,
            limit: 10,
            customLabels: myCustomLabels
        };
        delete req.query.page;
        Mobile.paginate(req.query, options, function (error, result) {
            if (error)
                next(new errors.makeErrFromCode(error.statusCode || 500, {statusCode: error.statusCode || 500,error}, error.message || "An error occurred."));
            else {
                for (let mobile of result.mobiles) {
                    //TODO price show
                    mobile.Price = mobile.price.toFixed(2);
                    delete mobile.price;
                }
                // TODO get 10 mobile from every single brand
                res.send(200, result.mobiles);
            }
        });
        // Mobile.model.find(req.query)
        //     .then(mobiles => {
        //         res.send(200, mobiles);
        //     })
        //     .catch(error => next(error));
    },
    show(req, res, next) {
        Mobile.findById(req.params.id)
            .then(mobile => {
                res.send(200, mobile);
            })
            .catch(error =>
                next(new errors.makeErrFromCode(error.statusCode || 500, {statusCode: error.statusCode || 500,error}, error.message || "An error occurred.")));
    },
    store(req, res, next) {
        /////TODO support adding a list of mobiles
        Mobile.create(req.body)
            .then(mobile => {
                res.send(201, {
                    message: "Mobile with id:" + mobile._id + " created successfully",
                    mobile
                });
            })
            .catch(error =>
                next(new errors.makeErrFromCode(error.statusCode || 500, {statusCode: error.statusCode || 500,error}, error.message || "An error occurred.")));
    },
    update: (req, res, next) => {
        Mobile.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true})
            .then(mobile => {
                res.send(200, {
                    message: "Mobile with id:" + mobile._id + " updated successfully",
                    mobile
                });
            })
            .catch(error =>
                next(new errors.makeErrFromCode(error.statusCode || 500, {statusCode: error.statusCode || 500,error}, error.message || "An error occurred.")))
    },
    destroy(req, res, next) {
        Mobile.findByIdAndDelete(req.params.id)
            .then(mobile => {
                res.send(200, {
                    message: "Mobile with id:" + mobile._id + " deleted successfully",
                    mobile
                });
            })
            .catch(error =>
                next(new errors.makeErrFromCode(error.statusCode || 500, {statusCode: error.statusCode || 500,error}, error.message || "An error occurred.")))
    }
};