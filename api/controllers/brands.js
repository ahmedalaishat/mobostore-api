/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/9/2020
 * Time: 2:40 PM
 */
const {Brand} = require('../models/Brand');
const errors = require("restify-errors");

module.exports = {
    index(req, res, next) {
        // const page = req.query.page || 1;
        // const myCustomLabels = {
        //     totalDocs: 'itemCount',
        //     docs: 'brands',
        //     limit: 'perPage',
        //     page: 'currentPage',
        //     nextPage: 'next',
        //     prevPage: 'prev',
        //     totalPages: 'pageCount',
        //     pagingCounter: 'slNo',
        //     meta: 'paginator'
        // };
        // const options = {
        //     select: "name url numberOfMobiles",
        //     page: page,
        //     limit: 10,
        //     customLabels: myCustomLabels
        // };
        Brand.find({}, function (error, brands) {
            if (error)
                next(new errors.makeErrFromCode(error.statusCode || 500, {
                        statusCode: error.statusCode || 500,
                        error
                    },
                    error.message || "Unknown error occurred."));
            else {
                res.send(200, brands);
            }
        });
    },
    show(req, res, next) {
        Brand.findById(req.params.id)
            .populate({
                path: 'mobiles',
                select: '_id name title url img img_url description price brand',
                model: 'Mobile',
                options: {
                    sort: {},
                    skip: 0,
                    limit: 10
                }
            })
            .then(result => {
                let brand = result.toJSON();
                for (let [key, value] of Object.entries(brand)) {
                    if (key === 'mobiles') {
                        brand.newestMobiles = value;
                        delete brand[key];
                    }
                }
                res.send(200, brand);
            })
            .catch(error => next(new errors.makeErrFromCode(error.statusCode || 500, {
                    statusCode: error.statusCode || 500,
                    error
                },
                error.message || "Unknown error occurred.")));
    },
    store(req, res, next) {
        /////TODO support adding a list of brands
        Brand.create(req.body)
            .then(brand => {
                res.send(201, {
                    message: "Brand with id:" + brand._id + " created successfully",
                    brand
                });
            })
            .catch(error => next(new errors.makeErrFromCode(error.statusCode || 500, {
                    statusCode: error.statusCode || 500,
                    error
                },
                error.message || "Unknown error occurred.")));
    },
    update: (req, res, next) => {
        Brand.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true})
            .then(brand => {
                res.send(200, {
                    message: "Brand with id:" + brand._id + " updated successfully",
                    brand
                });
            })
            .catch(error => next(new errors.makeErrFromCode(error.statusCode || 500, {
                    statusCode: error.statusCode || 500,
                    error
                },
                error.message || "Unknown error occurred.")));
    },
    destroy(req, res, next) {
        Brand.findByIdAndDelete(req.params.id)
            .then(brand => {
                res.send(200, {
                    message: "Brand with id:" + brand._id + " deleted successfully",
                    brand
                });
            })
            .catch(error => next(new errors.makeErrFromCode(error.statusCode || 500, {
                    statusCode: error.statusCode || 500,
                    error
                },
                error.message || "Unknown error occurred.")));
    }
};