/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 6/9/2020
 * Time: 6:17 PM
 */
const {User} = require('../models/User');
const errors = require("restify-errors");

module.exports = {
    signUp(req, res, next) {
        User.register(new User({
                username: req.body.username,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                gender: req.body.gender
            }),
            req.body.password, (error, user) => {
                if (error) {
                    next(new errors.makeErrFromCode(error.statusCode || 500, {
                        statusCode: error.statusCode || 500,
                        error
                    }, error.message || "Sign in failed!. Invalid username or password."));
                } else {
                    res.send(201, {
                        message: 'Sign up Successful!.',
                        user: user.toJSON()
                    });
                }
            });
    },
    signIn: async (req, res, next) => {
        await User.findById(req.user.id)
            .then((user) => {
                res.send(200, {
                    message: 'Sign in successful!.',
                    user: user.toJSON()
                });
            }, (error) =>
                next(new errors.makeErrFromCode(error.statusCode || 500, {
                    statusCode: error.statusCode || 500,
                    error
                }, error.message || "Sign in failed!. Invalid username or password.")))
    }
};