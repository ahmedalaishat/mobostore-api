/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 4/17/2020
 * Time: 1:39 AM
 */
const mongoose = require('mongoose');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt; // to extract token from request
const {User} = require('./models/User');
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
    database: () => {
        const url = process.env.MONGO_URL;
        const connect = mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true, useCreateIndex: true,
        });
        connect.then((db) => {
            console.log(db.connections[0].name + ' database connected correctly to the server');
        }, (error) => {
            console.log(error);
        });
    },
    auth: () => {
        // local strategy(only for login)
        passport.use(User.createStrategy());
        passport.serializeUser(User.serializeUser());
        passport.deserializeUser(User.deserializeUser());
        ////jwt strategy
        const jwtOpts = {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.SECRET_KEY
        };
        passport.use(new JwtStrategy(
            jwtOpts, (jwtPayload, cb) => User.findById(jwtPayload._id, (error, user) => user ? cb(null, user) : cb(error, false))
        ));
    },
    // createError: (error) => {
    //     console.log("a");
    //     if (error instanceof String) {
    //         console.log("b")
    //         return new errors.makeErrFromCode(500, {statusCode: 500}, error);
    //     }
    //     else {
    //         console.log("c")
    //         return new errors.makeErrFromCode(error.statusCode || 500, {
    //                 statusCode: error.statusCode || 500,
    //                 error
    //             },
    //             error.message || "Unknown error occurred.")
    //     }
    // }
};