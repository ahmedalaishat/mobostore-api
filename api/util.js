/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/13/2020
 * Time: 12:48 AM
 */

const getAcceptable = (brands, acceptable) => {
    return brands.filter(item => {
        if (!acceptable)
            return true;
        acceptable.brands = acceptable.brands.map(acc => acc.toLowerCase());
        if (acceptable.accept)
            return acceptable.brands.includes(item.name.toLowerCase());
        else return !acceptable.brands.includes(item.name.toLowerCase());
    });
};

const getFilter = query => {
    const filter ={};
    console.log("query",query)
    for (let [key, val] of Object.entries(query)) {
        console.log("val",val)
        if (typeof val === "string")
            filter[key] = {$regex: new RegExp('^' + val + '$', "i")};
        else
            filter[key] = val;
    }
    return filter;
};

const checkError = (result, res) => {
    if (result.status === 'failed') {
        console.log(result);
        res.send(result.error.statusCode || 500, result);
        return true;
    }
};

module.exports = {
    getFilter,
    getAcceptable,
    checkError,
};