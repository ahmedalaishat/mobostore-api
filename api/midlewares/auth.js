/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 6/9/2020
 * Time: 5:53 PM
 */
// jwt => generate token,verify user using token
const jwt = require('jsonwebtoken');
const passport = require('passport');
const errors = require('restify-errors');

module.exports = {
    authenticate: (req, res, next) => {
        passport.authenticate('local', function (err, user, info) {
            if (err) {
                return next(new errors.makeErrFromCode(500, {
                    statusCode: 500,
                    err
                }, err.message || "Sign in failed!. Invalid username or password."))
            }
            if (!user) {
                const err = new errors.makeErrFromCode(500, {statusCode: 500}, "Sign in failed!. Invalid username or password.");
                return next(err);
            }
            req.logIn(user, function (err) {
                if (err) {
                    const err = new errors.makeErrFromCode(500, {statusCode: 500}, "Sign in failed!. Invalid username or password.");
                    return next(err);
                }
                next();
            });
        })(req, res, next);
    },
    verifyUser: (req, res, next) => {
        passport.authenticate('jwt', {session: false}, (error, user, info) => {
            if (error) {
                const err = new errors.makeErrFromCode(error.statusCode || 401, {
                    statusCode: error.statusCode || 401,
                    error
                }, error.message || "An error occurred.");
                return next(err);
            }
            if (!user) {
                const err = new errors.makeErrFromCode(401, {statusCode: 401}, "You are not authenticated.");
                return next(err);
            }
            req.user = user;
            next();
        })(req, res, next);
    },
    verifyAdmin: (req, res, next) => {
        passport.authenticate('jwt', {session: false}, (error, user, info) => {
            if (error) {
                const err = new errors.makeErrFromCode(error.status || 401, {statusCode: error.statusCode || 401}, "An error occurred.");
                return next(err);
            }
            if (!user) {
                const err = new errors.makeErrFromCode(401, {statusCode: 401}, "You are not authenticated.");
                return next(err);
            }
            if (!user.admin) {
                const err = new errors.makeErrFromCode(403, {statusCode: 403}, "You are not authorized");
                return next(err);
            }
            req.user = user;
            next();
        })(req, res, next);
    },
    generateToken: (user) => {
        return jwt.sign(user, process.env.SECRET_KEY, {expiresIn: '60 days'});
    }
};