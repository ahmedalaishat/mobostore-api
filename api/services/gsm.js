/**
 * Created by Ahmed Al-Aishat on Aug/18/2022.
 * mobostore-api Project.
 * Copyright (c) 2022 Cloud Systems. All rights reserved.
 */
const gsmPages = require('./gsm-pages');
const createError = require('http-errors');
const util = require('../util');
const {Brand, forEachBrand, createBrandIfNotExists} = require('../models/Brand');
const {Mobile, forEachMobile, createMobileIfNotExists} = require('../models/Mobile');

const updateBrands = async (acceptable) => {
    const gsmResult = await gsmPages.brandsList();
    if (gsmResult.status === 'failed') {
        throw createError(gsmResult.error.statusCode || 500, gsmResult);
    }
    console.log("GSM response: ", {
        status: gsmResult.status,
        message: gsmResult.message,
        fetchedBrandsCount: gsmResult.brands.length
    });
    acceptable = acceptable || {
        accept: true,
        brands: ['samsung', 'Apple', 'Huawei', 'Xiaomi']
    };
    const brands = util.getAcceptable(gsmResult.brands, acceptable);
    console.log(`Creating ${brands.length} brands`)
    return await createBrandIfNotExists(brands)
}


const updateMobs = async (filter) => {
    const finalResult = []
    let created = 0
    let exist = 0
    let error = 0
    // filter or default one
    filter = filter ? util.getFilter(filter) : {}
    // {name: {$regex: new RegExp("apple", "i")}};
    await forEachBrand(filter, async brand => {
        console.log(`Fetching ${brand.name} mobiles list`)
        const mobiles = await gsmPages.mobilesList(brand.url, brand.name);
        console.log(mobiles.length + ' fetched');
        console.log("Creating mobiles list")
        const result = await createMobileIfNotExists(mobiles);
        console.log(result)
        // add existed mobiles
        if (brand.mobiles != null)
            result.mobiles.push(...brand.mobiles);
        try {
            const updateResult = await Brand.findOneAndUpdate({filter: {_id: brand._id}}, {$set: {mobiles: result.mobiles}}, {new: true});
            created += result.created
            exist += result.exist
            error += result.error
            finalResult.push({
                brand: brand.name,
                result,
            })
        } catch (e) {
            console.log("Error when updating the brand: " + brand.name)
        }
    })
    return {
        brands: finalResult,
        totalResult: {
            created,
            exist,
            error,
        }
    }
}

const updateDetails = async (filter) => {
    let updated = 0;
    let skipped = 0;
    let error = 0;
    let errorMessage = null;
    let completed = true;
    filter = filter ? util.getFilter(filter)
        : {/*brand: {$regex: new RegExp("samsung", "i")}*/};
    filter.full = false;
    await forEachMobile(filter, async mobile => {
        if (errorMessage != null) {
            skipped++
            return
        }
        const gsmResult = await gsmPages.mobile(mobile.url);
        if (gsmResult.status === 'failed') {
            console.log(`Error when fetching ${mobile.name} info: ${gsmResult.message}`)
            errorMessage = gsmResult.message
            completed = false
            return
        }
        console.log(`Updating ${mobile.name} info`)
        // const {url, ...set} = gsmResult.mobile
        try {
            await Mobile.findOneAndUpdate({_id: mobile._id}, {$set: gsmResult.mobile}, {new: true})
            console.log(`Mobile ${mobile.name} updated successfully`)
            updated++;
        } catch (e) {
            console.log("Error when updating mobile: " + mobile.name)
            console.log("Error: " + e.message)
            error++
        }
    })
    console.log(`${skipped} mobile skipped`)
    return {
        updated,
        skipped,
        error,
        errorMessage,
        completed,
    }
}

module.exports = {
    updateBrands,
    updateMobs,
    updateDetails,
}