/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/12/2020
 * Time: 5:11 AM
 */
// TODO search, review, reviews
const cheerio = require('cheerio');
const rp = require('request-promise');
const URI = process.env.URI;
const MARKER_URL = 'makers.php3';
const REVIEWS_URL = 'reviews.php3';

/**
 *  Get All Phone Brands Maker from GSM ARENA
 * @returns {Promise<T | {message: string, error: Object, status: string}>}
 */
const brandsList = () => {
    console.log('fetching' + MARKER_URL + ' for brand list');
    const requestOpts = {
        url: URI + MARKER_URL,
        headers: {
            "User-Agent": "request"
        }
    };
    return rp(requestOpts)
        .then(html => {
            $ = cheerio.load(html);
            let brands = [];
            let brandsTable = $('table').find('td');
            brandsTable.each((i, el) => {
                let brand = {
                    name: $(el).find('a').text().replace(' devices', '').replace(/[0-9]/g, ""),
                    numberOfMobiles: parseInt($(el).find('span').text().replace(' devices', '')),
                    url: $(el).find('a').attr('href'),
                };
                brands.push(brand)
            });
            return {
                status: 'success',
                message: brands.length + ' brand fetched.',
                brands
            };
        }).catch(error => {
            console.log(error);
            return {
                status: 'failed',
                message: 'Error failed fetching source.',
                error
            };
        });
};

/**
 *  Get brand mobiles page
 * @param url
 * @param brand
 * @returns {Promise<T | {error: {message: string, statusCode: number}}>}
 */
const mobilesPage = (url, brand) => {
    console.log('fetching ' + url + ' for brand (' + brand + ') mobiles');
    const requestOpts = {
        url: URI + url,
        headers: {
            "User-Agent": "request"
        }
    };
    return rp(requestOpts)
        .then(html => {
            $ = cheerio.load(html);
            let mobiles = [];

            // Get all list phone
            let mobilesTable = $('.makers').find('li');
            mobilesTable.each((i, el) => {
                let mobile = {
                    name: $(el).find('span').text(),
                    brand: brand || '',
                    img: $(el).find('img').attr('src'),
                    url: $(el).find('a').attr('href'),
                    description: $(el).find('img').attr('title')
                };
                mobiles.push(mobile)
            });

            // get next and prev page link
            let nextPage = $('a.pages-next').attr('href');
            let prevPage = $('a.pages-prev').attr('href');

            return {
                status: 'success',
                message: mobiles.length + ' ' + brand + ' mobile fetched .',
                mobiles,
                next: nextPage,
                prev: prevPage
            };
        }).catch(error => {
            console.log(error);
            return {
                status: 'failed',
                message: 'Error failed fetching source.',
                error
            };
        });
};

/**
 *  Get brand mobiles list
 * @param url
 * @param brand
 * @returns {Promise<[]>}
 */
const mobilesList = async (url, brand) => {
    const mobiles = [];
    while (url !== '#1') {
        const mobsPage = await mobilesPage(url, brand);
        mobiles.push(...mobsPage.mobiles);
        url = mobsPage.next || '#1';
    }
    return mobiles;
};

/** Get mobile detail
 * @param url
 * @returns {Promise<T | {message: string, error: Object, status: string}>}
 */
const mobile = (url) => {
    console.log('fetching ' + url + ' for mobile details');
    return rp({
        url: URI + url,
        headers: {
            "User-Agent": "request"
        }
    }).then(html => {
        $ = cheerio.load(html);
        // Get mobile detail
        let display_size = $('span[data-spec=displaysize-hl]').text();
        let display_res = $('div[data-spec=displayres-hl]').text();
        let camera_pixels = $('.accent-camera').text();
        let video_pixels = $('div[data-spec=videopixels-hl]').text();
        let ram_size = $('.accent-expansion').text();
        let chipset = $('div[data-spec=chipset-hl]').text();
        let battery_size = $('.accent-battery').text();
        let battery_type = $('div[data-spec=battype-hl]').text();
        let quick_spec = {
            display_size,
            display_res,
            camera_pixels,
            video_pixels,
            ram_size,
            chipset,
            battery_size,
            battery_type
        };

        let title = $('.specs-phone-name-title').text();
        let img = $('.specs-photo-main a img').attr('src');
        let img_url = $('.specs-photo-main a').attr('href');
        let specNode = $('table');
        let spec_detail = [];
        specNode.each((i, el) => {
            let specList = [];
            let category = $(el).find('th').text();
            let specN = $(el).find('tr');
            if (category !== '') {
                specN.each((index, ele) => {
                    let a = {
                        name: $('td.ttl', ele).text(),
                        value: $('td.nfo', ele).text()
                    };
                    if (a.name !== 'Price')
                        specList.push(a)
                });

                spec_detail.push({
                    category: category,
                    specs: specList
                })
            }
        });
        const mobile = {
            title,
            url,
            img,
            img_url,
            full: true,
            quick_spec,
            spec_detail,
        };
        return {
            status: 'success',
            message: title + ' details fetched successfully.',
            mobile
        };
    }).catch(error => {
        console.log(error);
        return {
            status: 'failed',
            message: error.error,
            error
        };
    });
};

module.exports = {
    brandsList,
    mobilesList,
    mobile,
};

/**
 *  search for a mobile
 */
const search = (req, res, next) => {
    rp({
        url: URI + 'results.php3?sQuickSearch=yes&sName=' + req.params.mobile,
        headers: {
            "User-Agent": "request"
        }
    }, (error, response, html) => {
        if (!error) {
            $ = cheerio.load(html);
            let json = [];

            // Get all list phone
            let phones = $('.makers').find('li');
            phones.each((i, el) => {
                let phone = {
                    name: $(el).find('span').html().split('<br>').join(' '),
                    img: $(el).find('img').attr('src'),
                    url: $(el).find('a').attr('href'),
                    description: $(el).find('img').attr('title')
                };
                json.push(phone)
            });


            res.send(json);
            next();
        } else {
            res.send({
                error: "Error failed fetching source"
            });
            next()
        }
    })
};


/**
 *  Get all reviews
 */
const indexReviews = (req, res, next) => {
    rp({
        url: URI + REVIEWS_URL,
        headers: {
            "User-Agent": "request"
        }
    }, (error, response, html) => {
        if (!error) {
            $ = cheerio.load(html);
            let json = [];

            // Get all list phone
            let reviews = $('.review-item');
            reviews.each((i, el) => {
                let review = {
                    title: $(el).find('.review-item-title').text(),
                    img: $(el).find('img').attr('src'),
                    url: $(el).find('a').attr('href'),
                    time: $(el).find('.meta-item-time').text()
                };
                json.push(review)
            });


            // get next and prev page link
            let nextPage = $('a.pages-next').attr('href');
            let prevPage = $('a.pages-prev').attr('href');

            data = {
                data: json,
                next: nextPage,
                prev: prevPage
            };

            res.send(data);
            next();
        } else {
            res.send({
                error: "Error failed fetching source"
            });
            next()
        }
    })
};

const showReview = (req, res, next) => {
    rp({
        url: URI + req.params.url,
        headers: {
            "User-Agent": "request"
        }
    }, (error, response, html) => {
        if (!error) {
            $ = cheerio.load(html);

            // Get all list phone
            let date = $('.dtreviewed').text();
            let reviewer = $('.reviewer').text();
            let title = $('.article-info-name').text();
            let review = $('#review-body').html();
            // get next and prev page link
            let nextPage = $('a.pages-next').attr('href');
            let prevPage = $('a.pages-prev').attr('href');

            let data = {
                date: date,
                reviewer: reviewer,
                review: review,
                title: title,
                next: nextPage,
                prev: prevPage
            };
            res.send(data);
            next();
        } else {
            res.send({
                error: "Error failed fetching source"
            });
            next()
        }
    })
};

