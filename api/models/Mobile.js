/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/8/2020
 * Time: 6:01 PM
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;
const Schema = mongoose.Schema;


const MobileSchema = new Schema({
    name: {
        type: String,
        unique: false,
        sparse: true
    },
    title: {
        type: String,
        unique: true,
        sparse: true
    },
    url: {
        type: String,
        required: true,
        unique: true
    },
    img: {
        type: String,
    },
    img_url: {
        type: String,
    },
    description: {
        type: String
    },
    price: {
        type: Currency,
        default: 100000000,
        min: 0
    },
    // isNew:{
    //     type:Boolean,
    //     default:false,
    // },
    full: {type: Boolean, default: false},
    brand: {type: String},
    quick_spec: {
        display_size: {type: String},
        display_res: {type: String},
        camera_pixels: {type: String},
        video_pixels: {type: String},
        ram_size: {type: String},
        chipset: {type: String},
        battery_size: {type: String},
        battery_type: {type: String},
    },
    spec_detail: [{
        category: {type: String},
        specs: [{name: {type: String}, value: {type: String}}]
    }],
}, {timestamps: true});

MobileSchema.plugin(mongoosePaginate);
const Mobile = mongoose.model('Mobile', MobileSchema);

const forEachMobile = (filter, fn) => Mobile.find(filter).cursor().eachAsync(fn, {parallel: 2});

/**
 * update mobile details
 * @param mobile
 * @param set
 * @returns {Promise<void>}
 */
const updateOneMobile = async (mobile, set) => {
    let promise;
    if (typeof mobile === "number")
        promise = Mobile.findByIdAndUpdate(mobile, {$set: set}, {new: true});
    else if (mobile.filter != null)
        promise = Mobile.findOneAndUpdate(mobile.filter, {$set: set}, {new: true});
    else
        throw new Error('mobile id or filter not found');
    return promise.then(mobile => {
        return {
            status: "success",
            message: "one mobile updated successfully.",
            mobile: {name: mobile.name},
        };
    }).catch(error => {
        return {
            status: "failed",
            message: "an error occurred while updating.",
            error
        };
    });
};

/**
 * update all mobiles details
 * @returns {Promise<{count: number, message: string, status: string}|{message: string, error: Object, status: string}>}
 */
const createMobile = (mobiles) => {
    return Mobile.create(mobiles)
        .then(mobiles => {
            return {
                status: 'success',
                message: mobiles.length || 'One' + ' mobile created successfully.',
                mobiles: mobiles.length ? mobiles : [mobiles]
            }
        }).catch(error => {
            return {
                status: 'failed',
                message: 'an error occurred while adding mobiles to brand.',
                error
            }
        });
};


const createMobileIfNotExists = async (mobiles) => {
    let error = 0;
    let exist = 0;
    let added = [];
    // store acceptable brands
    for (let mobile of mobiles) {
        await Mobile.findOne({url: mobile.url}).then(async result => {
            if (result == null) {
                await Mobile.create(mobile)
                    .then(createdMobile => {
                        added.push(createdMobile);
                        console.log('One mobile created successfully.')
                    }).catch(err => {
                        error++
                        console.log("Error occurred while adding mobile")
                    });
            } else {
                exist++
                console.log(`Mobile ${mobile.name} already exist`)
            }
        }).catch(err => {
            error++
            console.log("Error occurred while adding mobiles to brand")
        })
    }
    return {
        created: added.length,
        exist,
        error,
        mobiles: added,
    }
};

module.exports = {
    Mobile,
    forEachMobile,
    createMobile,
    createMobileIfNotExists,
    updateOneMobile,
};