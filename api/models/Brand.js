/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 5/8/2020
 * Time: 6:00 PM
 */
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;


const BrandSchema = new Schema({
    name: {
        type: String,
        unique: false,
        required: true
    },
    numberOfMobiles: {
        type: Number,
        required: true
    },
    url: {
        type: String,
        required: true,
        unique: true
    },
    mobiles: {
        type: [{
            type: Schema.Types.ObjectID,
            ref: 'Mobile',
            unique: true
        }],
        unique: false,
    }
}, {timestamps: true});

BrandSchema.plugin(mongoosePaginate);

const Brand = mongoose.model('Brand', BrandSchema);

const forEachBrand = (filter, fn) => Brand.find(filter).cursor().eachAsync(fn, {parallel: 2});

const createBrand = (brands) => {
    return Brand.create(brands)
        .then(result => {
            return {
                status: 'success',
                message: result.length + " brand created successfully.",
                brands: result
            };
        })
        .catch(error => {
            return {
                status: 'failed',
                message: "Error occurred while storing brands.",
                error,
            };
        })
};

const createBrandIfNotExists = async (brands) => {
    let created = 0;
    let exist = 0;
    let error = 0;
    // store acceptable brands
    for (let brand of brands) {
        await Brand.findOne({name: brand.name}).then(async result => {
            if (result == null) {
                await Brand.create(brands)
                    .then(() => {
                        created++;
                        console.log("Brand created successfully: " + brand.name)
                    })
                    .catch(error => {
                        error++
                        console.log("Error occurred while creating brand: " + error.message)
                    })
            } else {
                exist++
                console.log("Brand already exist: " + brand.name)
            }
        }).catch(error => {
            error++
            console.log("Error occurred while fetching brand: " + error.message)
        })
    }
    return {created, exist, error};
};

const updateOneBrand = (brand, set) => {
    let promise;
    if (typeof brand === "number")
        promise = Brand.findByIdAndUpdate(brand, {$set: set}, {new: true});
    else if (brand.filter != null)
        promise = Brand.findOneAndUpdate(brand.filter, {$set: set}, {new: true});
    else
        throw new Error('brand id or filter not found');
    return promise
        .then(result => {
            return {
                status: 'success',
                message: (result || brand) + ' updated successfully, ' + set.mobiles.length + ' mobile added.',
                result
                // mobiles: set.mobiles.map(mobile => mobile.name),
            }
        })
        .catch(error => {
            return {
                status: 'failed',
                message: 'an error occurred while adding mobiles to ' + brand.name + ' brand.',
                error
            }
        });
};

module.exports = {
    Brand,
    forEachBrand,
    createBrand,
    createBrandIfNotExists,
    updateOneBrand
};