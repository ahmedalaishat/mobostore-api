/**
 * Created by Ahmed Al-Aishat on Aug/19/2022.
 * mobostore-api Project.
 * Copyright (c) 2022 Cloud Systems. All rights reserved.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HistorySchema = new Schema({
    result: {
        type: Object,
    },
}, {timestamps: true});

const History = mongoose.model('History', HistorySchema);

module.exports = History