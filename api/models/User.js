/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 6/9/2020
 * Time: 1:52 PM
 */
const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const auth = require('../midlewares/auth');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    username: {
        type: String,
        lowercase: true,
        unique: true,
        required: [true, "can't be blank"],
        match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
        index: true
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: [true, "can't be blank"],
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    admin: {
        type: Boolean,
        default: false
    },
    gender: {
        type: String,
        enum: ['M', 'F'],
        required: true
    },
    bio: String,
    image: String,
}, {timestamps: true});

UserSchema.plugin(passportLocalMongoose, {
    selectFields: 'username firstName lastName email password gender ',
    usernameField: 'username',
    usernameQueryFields: ['email']
});
UserSchema.plugin(mongoosePaginate);

UserSchema.methods.toJSON = function () {
    return {
        username: this.username,
        firstName: this.firstName,
        lastName: this.lastName,
        gender: this.gender,
        email: this.email,
        token: auth.generateToken({_id: this._id}) || "",
        bio: this.bio || "",
        image: this.image || ""
    };
};

const User = mongoose.model('User', UserSchema);

module.exports = {
    User
};